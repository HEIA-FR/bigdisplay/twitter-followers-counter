[![Go Report Card](https://goreportcard.com/badge/gitlab.com/HEIA-FR/bigdisplay/twitter-followers-counter)](https://goreportcard.com/report/gitlab.com/HEIA-FR/bigdisplay/twitter-followers-counter)
[![license](https://img.shields.io/badge/license-Apache--2.0-green.svg)](https://gitlab.com/HEIA-FR/bigdisplay/twitter-followers-counter)

# Twitter Followers Counter
